# README

Commandes lancées :

Question 2.2 : 

* `rails server`

Question 2.3 :

* `rails generate controller HomeController`

Question 3 :

* `rails generate model Creature name:string health_points:integer`
* La migration n'est pas lancée sur la base
* `rails db:migrate`

Question 3.1 :

Je teste la creature dans une console rails que je lance avec `rails console` :

```ruby
❯ rails console
Loading development environment (Rails 7.0.4.3)
irb(main):001:0> c = Creature.create(name: "Thrall", health_points: 489)
  TRANSACTION (0.1ms)  begin transaction
  Creature Create (0.4ms)  INSERT INTO "creatures" ("name", "health_points", "created_at", "updated_at") VALUES (?, ?, ?, ?)  [["name", "Thrall"], ["health_points", 489], ["created_at", "2023-03-26 13:19:57.492017"], ["updated_at", "2023-03-26 13:19:57.492017"]]
  TRANSACTION (0.9ms)  commit transaction
=>
#<Creature:0x00000001070374f8
...
irb(main):002:0> c.to_label
=> "Thrall (489)"
```

Question 3.2 :

* `bundle add random_name_generator` pour ajouter la gemme au gemfile, effectuer la résolution de dépendance et installer la nouvelle gemme

* `rails db:seed`

## TP4

Question 2.5 :

* J'ajoute une créature avec des PVs à 0 :

```ruby
❯ rails console
Loading development environment (Rails 7.0.4.3)
irb(main):001:0> Creature.create(name: 'Zombie', health_points: 0)
  TRANSACTION (0.1ms)  begin transaction
  Creature Create (0.4ms)  INSERT INTO "creatures" ("name", "health_points", "created_at", "updated_at") VALUES (?, ?, ?, ?)  [["name", "Zombie"], ["health_points", 0], ["created_at", "2023-03-26 14:00:56.413028"], ["updated_at", "2023-03-26 14:00:56.413028"]]
  TRANSACTION (0.9ms)  commit transaction
=>
#<Creature:0x00000001104b9310
 id: 27,
 name: "Zombie",
 health_points: 0,
 created_at: Sun, 26 Mar 2023 14:00:56.413028000 UTC +00:00,
 updated_at: Sun, 26 Mar 2023 14:00:56.413028000 UTC +00:00>
```

* J'essaie de renommer la créature dans postman et je vérifie que son nom ne change pas

* J'ai bien une 404 et son nom n'a pas changé

Question 2.6 :

* Je génère la migration avec `rails g migration AddSizeColumnToCreatures size:integer`

* `rails db:migrate`

* J'ajoute le callback comme dans le sujet du TP pour setter la size à la création, je teste en console :

```ruby
irb(main):001:0> Creature.create(name: 'Big Chongus', health_points: 25)
  TRANSACTION (0.1ms)  begin transaction
  Creature Create (0.4ms)  INSERT INTO "creatures" ("name", "health_points", "created_at", "updated_at", "size") VALUES (?, ?, ?, ?, ?)  [["name", "Big Chongus"], ["health_points", 35], ["created_at", "2023-03-26 14:07:03.876318"], ["updated_at", "2023-03-26 14:07:03.876318"], ["size", 2]]
  TRANSACTION (0.6ms)  commit transaction
=>
#<Creature:0x0000000109a96d70
 id: 28,
 name: "Big Chongus",
 health_points: 25,
 created_at: Sun, 26 Mar 2023 14:07:03.876318000 UTC +00:00,
 updated_at: Sun, 26 Mar 2023 14:07:03.876318000 UTC +00:00,
 size: "big">
```

Toutes les créatures crées auront dorénavant une taille. 

Maintenant il faut faire une migration pour mettre à jour celles en base.

* `rails g migration SetSizeOnCreatures`

* Après avoir écrit la mise à jour, j'applique avec : `rails db:migrate`

* Je teste en console en regardant une vieille créature : 

```ruby
irb(main):012:0> Creature.first
  Creature Load (0.5ms)  SELECT "creatures".* FROM "creatures" ORDER BY "creatures"."id" ASC LIMIT ?  [["LIMIT", 1]]
=>
#<Creature:0x0000000138c856e8
 id: 1,
 name: "Thrall",
 health_points: 489,
 created_at: Sun, 26 Mar 2023 13:19:57.492017000 UTC +00:00,
 updated_at: Sun, 26 Mar 2023 14:11:33.479248000 UTC +00:00,
 size: "giant">
```

Question 3 :

* Je génère la migration pour ajouter les combats avec : `rails g migration CreateCombat result:integer name:string left_fighter:references right_fighter:references winner:references`

Cette migration sera pré-chargée avec :

```ruby
class CreateCombat < ActiveRecord::Migration[7.0]
  def change
    create_table :combats do |t|
      t.integer :result
      t.string :name
      t.references :left_fighter, null: false, foreign_key: true
      t.references :right_fighter, null: false, foreign_key: true
      t.references :winner, null: false, foreign_key: true

      t.timestamps
    end
  end
end
```

Il va falloir préciser vers quelle table doit pointer la foreign key, pour ça on remplace `foreign_key: true` par `foreign_key: { to_table: 'creatures' }`.

On remplacera aussi le `null: false` de la référence `winner` pour supporter de ne pas en avoir.

Question 3.1 : 

On ajoute la méthode baston! sur les combats et on teste en console. La méthode modifie les instances de créature mais ne sauvegarde pas.

Pour une domination :

```ruby
❯ rails console
Loading development environment (Rails 7.0.4.3)
irb(main):001:0> c1 = Creature.first
...
irb(main):003:0> c1.to_label
=> "Thrall (489)"
irb(main):004:0> c2 = Creature.last
...
irb(main):005:0> c2.to_label
=> "Big Chongus (35)"
irb(main):006:0> combat = Combat.new(name: "L'orc vs le lapin", left_fighter: c1, right_fighter: c2)
=> #<Combat:0x0000000111817d58 id: nil, result: nil, name: "L'orc vs le lapin", left_fighter_id: 1, right_fighter_id: 28, winner_id: nil, created_at: nil, updated_at: nil>
irb(main):007:0> combat.baston!
=> :domination
irb(main):008:0> combat.winner
=>
#<Creature:0x00000001274b5e60
 id: 1,
 name: "Thrall",
 health_points: 454,
 created_at: Sun, 26 Mar 2023 13:19:57.492017000 UTC +00:00,
 updated_at: Sun, 26 Mar 2023 14:11:33.479248000 UTC +00:00,
 size: "giant">
irb(main):009:0> combat.right_fighter
=>
#<Creature:0x00000001274bffa0
 id: 28,
 name: "Big Chongus",
 health_points: 0,
 created_at: Sun, 26 Mar 2023 14:07:03.876318000 UTC +00:00,
 updated_at: Sun, 26 Mar 2023 14:07:03.876318000 UTC +00:00,
 size: "giant">
```

Pour un draw :

```ruby
irb(main):011:0> c2.reload # permet de recharger le record depuis la base
...
=>
#<Creature:0x00000001274bffa0
 id: 28,
 name: "Big Chongus",
 health_points: 35,
 created_at: Sun, 26 Mar 2023 14:07:03.876318000 UTC +00:00,
 updated_at: Sun, 26 Mar 2023 14:07:03.876318000 UTC +00:00,
 size: "giant">
irb(main):012:0> c3 = Creature.create!(name: "Big Chungus", health_points: 35)
...
irb(main):013:0> combat2 = Combat.new(name: "Le fake vs le vrai", left_fighter: c2, right_fighter: c3)
=> #<Combat:0x00000001077212a0 id: nil, result: nil, name: "Le fake vs le vrai", left_fighter_id: 28, right_fighter_id: 29, winner_id: nil, created_at: nil, updated_at: ...
irb(main):014:0> combat2.baston!
=> :draw
irb(main):015:0> combat2.winner
=> nil
irb(main):016:0> c2.to_label
=> "Big Chongus (0)"
irb(main):017:0> c3.to_label
=> "Big Chungus (0)"
```
