Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  get '/dice-rolls/:dice_type/(:rolls_count)', to: "dice_rolls#rolls"

  # CRUD Créatures
  get '/creatures', to: 'creatures#index'
  get '/creatures/:id', to: 'creatures#show'
  post '/creatures', to: 'creatures#create'
  put '/creatures/:id', to: 'creatures#update'
  delete '/creatures/:id', to: 'creatures#destroy'

  # Combats
  get '/combats', to: 'combats#index'
  post '/combats', to: 'combats#create'

  # Defines the root path route ("/")
  root 'home#welcome'
end
