# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

rng = RandomNameGenerator.new

nb_creatures = rand(10..20)
nb_creatures.times do
  nb_syllables = rand(2..4)

  c = Creature.create(name: rng.compose(nb_syllables), health_points: rand(6..50))
  puts "Création de #{c.to_label}"
end

nb_combats = rand(10..20)
nb_combats.times do |i|
  left_fighter, right_fighter = Creature.alive.sample(2)

  next unless left_fighter && right_fighter

  combat = Combat.new(left_fighter: left_fighter, right_fighter: right_fighter, name: "Auto-généré #{i + 1}")
  combat.baston!
  combat.left_fighter.save!
  combat.right_fighter.save!
  combat.save!
  puts combat.to_label
end
