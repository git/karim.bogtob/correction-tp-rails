class CreateCombat < ActiveRecord::Migration[7.0]
  def change
    create_table :combats do |t|
      t.integer :result
      t.string :name
      t.references :left_fighter, null: false, foreign_key: { to_table: 'creatures' }
      t.references :right_fighter, null: false, foreign_key: { to_table: 'creatures' }
      t.references :winner, null: true, foreign_key: { to_table: 'creatures' }

      t.timestamps
    end
  end
end
