class AddSizeColumnToCreatures < ActiveRecord::Migration[7.0]
  def change
    add_column :creatures, :size, :integer
  end
end
