class SetSizeOnCreatures < ActiveRecord::Migration[7.0]
  def change
    Creature.all.each do |creature|
      creature.size = case creature.health_points
        when 0..10
          :small
        when 11..30
          :big
        else
          :giant
      end
      creature.save!
    end    
  end 
end
