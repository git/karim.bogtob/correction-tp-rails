class CreaturesController < ApplicationController
  SHOWABLE_ATTRIBUTES = [:id, :name, :health_points]

  def index
    @creatures = Creature.all
    render json: @creatures.as_json(only: SHOWABLE_ATTRIBUTES)
  end
  
  def show
    @creature = Creature.find(params[:id])
    render json: @creature.as_json(only: SHOWABLE_ATTRIBUTES)
  rescue ActiveRecord::RecordNotFound
    render json: {}, status: 404
  end

  def create
    @creature = Creature.new(create_or_update_params)
    @creature.health_points = rand(3..30)
    @creature.save!
    render json: @creature.as_json(only: SHOWABLE_ATTRIBUTES)
  end

  def update
    @creature = Creature.alive.find(params[:id])
    @creature.update!(create_or_update_params)
    render json: @creature.as_json(only: SHOWABLE_ATTRIBUTES)
  rescue ActiveRecord::RecordNotFound
    render json: {}, status: 404
  end

  def destroy
    @creature = Creature.find(params[:id])
    @creature.destroy!
    head :no_content
  end

  private
  def create_or_update_params
    params.require(:creature).permit(:name)
  end
end