class CombatsController < ApplicationController
  COMBATS_RENDER_CONFIG = { 
    left_fighter: { only: CreaturesController::SHOWABLE_ATTRIBUTES },
    right_fighter: { only: CreaturesController::SHOWABLE_ATTRIBUTES },
    winner: { only: CreaturesController::SHOWABLE_ATTRIBUTES },
  }

  def index
    @combats = Combat.all

    # on supporte uniquement les réels résultats possibles qui sont enregistrés sur le modèle Combat
    if Combat.results.include?(params[:result])
      @combats = @combats.where(result: params[:result])
    end

    # Si on founit une query, on l'utilise pour la requête like
    if params.include?(:query)
      @combats = @combats.where("name LIKE ?", "%#{params[:query]}%")
    end

    render json: @combats.as_json(include: COMBATS_RENDER_CONFIG)
  end

  def create
    left_fighter = Creature.find(params[:left_fighter_id])
    right_fighter = Creature.find(params[:right_fighter_id])

    @combat = Combat.new(create_params)
    @combat.left_fighter = left_fighter
    @combat.right_fighter = right_fighter
    @combat.baston!
    @combat.left_fighter.save!
    @combat.right_fighter.save!
    @combat.save!

    render json: @combat.as_json(include: COMBATS_RENDER_CONFIG)
  rescue ActiveRecord::RecordNotFound
    render json: {}, status: 404
  end

  private
  def create_params
    params.require(:combat).permit(:name)
  end
end