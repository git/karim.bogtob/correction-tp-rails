class DiceRollsController < ApplicationController
  VALID_DICE_TYPES = %w[d2 d4 d6 d8 d10 d20 d100]

  def rolls
    # si c'est pas un nombre, to_i renverra 0
    requested_rolls_count = params[:rolls_count].to_i
    # si le nombre de dés demandé est pas supérieur à 0, on utilise 1 comme valeur par défaut
    rolls_count = (requested_rolls_count > 0) ? requested_rolls_count : 1
    
    unless VALID_DICE_TYPES.include?(params[:dice_type])
      render json: {}, status: 404
    end

    dice_type = params[:dice_type]
    rolls = []
    
    rolls_count.times do 
      # on a une méthode dédiée pour lancer un dé
      rolls.push(roll_dice(dice_type))
    end

    render json: {
      dice: dice_type,
      rolls: rolls,
    }
  end

  private

  def roll_dice(dice_type)
    # ex: on passe de "d100" à "100" puis à l'entier 100 avec le to_i
    upper_bound = dice_type[1..].to_i
    # rand de 1 à la taille du dé 
    rand(1..upper_bound)
  end
end