# Contrôleur et vue générée avec `rails generate controller HomeController`
class HomeController < ApplicationController
  def welcome
    @creatures_count = Creature.count
    @creatures = Creature.all
  end
end
