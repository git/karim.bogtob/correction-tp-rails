class Creature < ApplicationRecord
  scope :alive, -> { where('health_points > 0') }

  enum :size, [:small, :big, :giant]

  validates :name, presence: true
  validates :health_points, numericality: { greater_than_or_equal_to: 0 }

  def alive?
    self.health_points > 0
  end

  def to_label
    "#{name} (#{health_points})"
  end

  def get_hit(damage)
    self.health_points -= damage
    self.health_points = 0 if self.health_points < 0
  end

  before_create do
    self.size = case health_points
      when 0..10
        :small
      when 11..30
        :big
      else
        :giant
    end    
  end 
end
