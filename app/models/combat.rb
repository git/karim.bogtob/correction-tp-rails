class Combat < ApplicationRecord
  enum :result, [:draw, :domination]

  belongs_to :left_fighter, class_name: 'Creature'
  belongs_to :right_fighter, class_name: 'Creature'
  belongs_to :winner, class_name: 'Creature', optional: true

  def to_label
    return "Invalid combat" unless valid?
    "Combat '#{name}' entre #{left_fighter.name} et #{right_fighter.name}"
  end

  def baston!
    return if left_fighter.nil? || right_fighter.nil?

    # on récupère les PVs d'abord, sinon l'ordre de l'attaque va affecter le résultat
    left_hp = left_fighter.health_points
    right_hp = right_fighter.health_points

    left_fighter.get_hit(right_hp)
    right_fighter.get_hit(left_hp)

    if left_fighter.alive?
      self.winner = left_fighter
      self.result = :domination
    elsif right_fighter.alive?
      self.winner = right_fighter
      self.result = :domination
    else
      self.result = :draw
    end
  end
end
